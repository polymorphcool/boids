# generation of a list of random number where everyone appears one and only one time
# see https://primes.utm.edu/lists/small/10000.txt for the first 10000 primes
# formula: X1 = (a*X0+b)%m, where a, b & m are primes

import time

EXPORT = 	True
prime_a = 	int(102139)
prime_b = 	int(91331)
total = 	int(16301)

current = int(time.time())

print( "prime_a:", 	prime_a, 	'type:', type(prime_a) )
print( "prime_b:", 	prime_b, 	'type:', type(prime_b) )
print( "total:", 	total, 		'type:', type(total) )
print( "current:", 	current, 	'type:', type(current) )

duplicates = []
rands = []
for i in range( 0, total-1 ):
	current = ( prime_a * current + prime_b ) % total
	# already in the list?
	if current in rands:
		print(i)
		duplicates.append( current )
	else:
		rands.append( current )

print( "duplicates: ", len(duplicates) )
print( "uniques: ", len(rands) )

if EXPORT:
	f = open( 'random_'+str(total-1)+'.txt', 'w' )
	for r in rands:
		f.write( str(r) + '\n' )
	f.close()
