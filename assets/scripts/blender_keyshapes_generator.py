import bpy, mathutils, math

'''
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 Copyright (c) 2021 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
'''

'''
This script bake a serie of shapekeys from a skeletal animation.
To set this up, you need:
- a skeletal animation
- a copy of the mesh without an armature

Once done, you have to adapt these global variables:
- SRC: mesh attached to the armature;
- SRC_MOD: name of the armature modifier attached to the source mesh;
- DST: destination mesh, outside of the armature;
- CONFIG: a list of dictionaries defining:
- - prefix: prefix of shape keys;
- - in: first frame to bake;
- - out: last frame to bake;
- - gap: how many frames between 2 bakes.

the script will generate:
- one shape key every x frame,
- keyframes in timeline for each shape key to preview the animation

BAKE_GAP: gap between 2 config in the timeline
JUST_CLEAN: set to true to clean up the shape keys in DST
'''


print( '>>> blender_keyshapes_generator STARTS' )

SRC = 		bpy.data.objects['remesh_8_combined_Remeshed']
SRC_MOD = 	"Armature.001"
DST = 		bpy.data.objects['avatar']
CONFIG = [
    { 'prefix':'hanging', 	'in': 678, 	'out': 750, 	'gap': 3 },
    { 'prefix':'prout', 	'in': 100, 	'out': 120, 	'gap': 2 },
    { 'prefix':'patapouf', 	'in': 10, 	'out': 90, 		'gap': 3 }
]
BAKE_GAP = 20
JUST_CLEAN =	False

src_mesh = SRC.data
dst_mesh = DST.data
timeline_offset = 0

# cleanup of ALL shapekeys in SRC & DST
def clean_up( obj=None, start_at = -1 ):
	
	if obj == None:
		return
	
	bpy.ops.object.select_all(action='DESELECT')
	bpy.context.view_layer.objects.active = obj
	obj.select_set(True)
	if obj.data.shape_keys != None:
		if start_at == -1:
			obj.active_shape_key_index = 0
			bpy.ops.object.shape_key_remove(all=True)
		else:
			while len(obj.data.shape_keys.key_blocks) > start_at:
				obj.active_shape_key_index = start_at
				bpy.ops.object.shape_key_remove(all=False)
	
	bpy.ops.object.select_all(action='DESELECT')

def render_digit( i ):
	out = str(i)
	while len( out ) < 4:
		out = '0'+out
	return out

def generate( conf ):
	
	global timeline_offset
	
	#- moving to fame
	
	kblocks_start = len(dst_mesh.shape_keys.key_blocks)
	
	bid = 0
	for i in range( conf['in'], conf['out'] + 1, conf['gap'] ):
		
		bpy.context.scene.frame_set(i)

		# create a new shape kay from modifier in source
		bpy.ops.object.select_all(action='DESELECT')
		bpy.context.view_layer.objects.active = SRC
		SRC.select_set(True)
		bpy.ops.object.modifier_apply_as_shapekey(keep_modifier=True, modifier=SRC_MOD)

		if src_mesh.shape_keys != None:
			
			last_sk = len( src_mesh.shape_keys.key_blocks )-1
			src_sk = src_mesh.shape_keys.key_blocks[last_sk]
			
			# creation of a keyshape in destination
			bpy.ops.object.select_all(action='DESELECT')
			bpy.context.view_layer.objects.active = DST
			DST.select_set(True)
			bpy.ops.object.shape_key_add(from_mix=False)
			kblocks = dst_mesh.shape_keys.key_blocks
			sk = kblocks[len(kblocks)-1]
			sk.name = conf['prefix'] + '_' + render_digit(bid)

			# copying last source keyshape in destination
			for i in range(0,len(dst_mesh.vertices)):
				sk.data[i].co = SRC.matrix_world @ src_sk.data[i].co
			
			bid += 1

	#- animation preview
	kblocks = dst_mesh.shape_keys.key_blocks
	kblocks_end = len(kblocks)
	fid = 0
	for i in range( kblocks_start, kblocks_end ):
			sk = kblocks[i]
			f = fid*conf['gap']
			sk.value = 0
			sk.keyframe_insert("value", frame= timeline_offset+f-conf['gap'])
			sk.value = 1
			sk.keyframe_insert("value", frame= timeline_offset+f)
			sk.value = 0
			sk.keyframe_insert("value", frame= timeline_offset+f+conf['gap'])
			fid += 1
	
	timeline_offset += BAKE_GAP
	timeline_offset += bid * conf['gap']
	
	print( conf['prefix'], ' baked, ', bid, ' shape keys generated' )
	print( timeline_offset )

# generation of shapekeys
def process():
	
	# creating a basis
	bpy.ops.object.shape_key_add(from_mix=False)
	dst_mesh.shape_keys.key_blocks[0].name = '000_std'

	# copying source to destination

	#- setup
	dst_mesh.shape_keys.use_relative = True
	vnum = len(src_mesh.vertices)

	#- storing current shapekeys in source
	original_sk_count = 0
	if src_mesh.shape_keys != None:
		original_sk_count = len( src_mesh.shape_keys.key_blocks )

	#- looping over config
	for conf in CONFIG:
		print( 'baking: ', conf['prefix'] )
		generate( conf )

	if original_sk_count > 0:
		clean_up( SRC, original_sk_count )
		rm = len( src_mesh.shape_keys.key_blocks ) - original_sk_count
		print( "shape keys to remove", rm )
	else:
		clean_up( SRC )
		pass

	bpy.ops.object.select_all(action='DESELECT')
	bpy.context.view_layer.objects.active = DST
	DST.select_set(True)

# RUNNING

#- switching to OBJECT MODE
bpy.ops.object.mode_set(mode='OBJECT')
clean_up( DST )
if not JUST_CLEAN:
	process()

print( '>>> blender_keyshapes_generator GEDAAN' )
