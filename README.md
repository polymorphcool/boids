# boids demo

![](https://img.itch.zone/aW1hZ2UvODc1ODI5LzUwOTI0NTQucG5n/original/8dEi0m.png)

this project is a demo of the [boids module](https://gitlab.com/polymorphcool/godot_module_boids) currently in development
