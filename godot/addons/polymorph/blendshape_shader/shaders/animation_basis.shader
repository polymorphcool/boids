shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform sampler2D animation: hint_black;
uniform float animation_weight: hint_range(-1,2);
uniform float animation_line = 1;
uniform float animation_frames = 1;
uniform float animation_frame_gap = 1;

uniform float inflate = 0;
uniform sampler2D inflate_tex: hint_white;

uniform float time = 0.;
const float pi = 3.141592653589;

// getting frame indices
void compute_frames( float t, float gap, float lines, float frame_count, inout float fprev, inout float fnext, inout float fmix, inout float ratio ) {
	float anim_frame = (t/gap) * frame_count;
	fprev = float(int(anim_frame));
	fnext = fprev + 1.0;
	fmix = anim_frame-fprev;
	fprev *= lines / frame_count; 
	fnext *= lines / frame_count; 
	ratio = float(frame_count * lines) * 2.;
}

// scaling UV2 y to match animation
vec2 compute_uv( vec2 uv, float ratio ) {
	vec2 anim_uv = uv;
	anim_uv.y /= ratio;
	anim_uv.y += 0.5 / ratio;
	return anim_uv;
}

// getting the frames for vertex or normal, depending on uv
vec3 extract_vec3( sampler2D tex, vec2 uv, float fprev, float fnext, float fmix ) {
	vec3 v_prev = texture( tex, uv + vec2(0., fprev ) ).xyz;
	vec3 v_next = texture( tex, uv + vec2(0., fnext ) ).xyz;
	return mix( v_prev, v_next, fmix );
}

void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	vec3 origin_vertex = VERTEX;
	/* animation specific process -- starts */
	float frame_prev;
	float frame_next;
	float frame_mix;
	float yratio;
	compute_frames( time, animation_frame_gap, animation_line, animation_frames, frame_prev, frame_next, frame_mix, yratio );
	vec2 anim_uv = compute_uv( UV2, yratio );
	vec3 v_mix = extract_vec3( animation, anim_uv, frame_prev, frame_next, frame_mix );
	vec3 n_mix = extract_vec3( animation, anim_uv + vec2(0.,animation_line / yratio), frame_prev, frame_next, frame_mix );
	VERTEX += v_mix * animation_weight;
	NORMAL = mix(NORMAL, n_mix, animation_weight);
	/* animation specific process -- ends */
//	VERTEX += NORMAL * inflate * texture(inflate_tex,vec2(origin_vertex.y*.5,0.5)).x;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
}
