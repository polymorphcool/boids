shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform sampler2D animation : hint_black;
uniform float animation_speed = 1.0;

uniform sampler2D key0_vertex : hint_black;
uniform sampler2D key0_normal : hint_black;
uniform float key0_weight = 0.0;
uniform sampler2D key1_vertex : hint_black;
uniform sampler2D key1_normal : hint_black;
uniform float key1_weight = 0.0;
uniform sampler2D key2_vertex : hint_black;
uniform sampler2D key2_normal : hint_black;
uniform float key2_weight = 0.0;
uniform sampler2D key3_vertex : hint_black;
uniform sampler2D key3_normal : hint_black;
uniform float key3_weight = 0.0;
uniform sampler2D key4_vertex : hint_black;
uniform sampler2D key4_normal : hint_black;
uniform float key4_weight = 0.0;

void vertex() {
	
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	
	vec4 a0 = texture( animation, vec2(TIME*animation_speed,.25) );
	vec4 a1 = texture( animation, vec2(TIME*animation_speed,.75) );
	
	float k0w = key0_weight + a0.r;
	float k1w = key1_weight + a0.g;
	float k2w = key2_weight + a0.b;
	float k3w = key3_weight + a1.r;
	float k4w = key4_weight + a1.g;
	
	float totalw = 0.0;
	vec3 v = texture( key0_vertex, UV2 ).xyz * 	k0w;
	vec3 n = texture( key0_normal, UV2 ).xyz * 	k0w;
	totalw += 									k0w;
	v += texture( key1_vertex, UV2 ).xyz *		k1w;
	n += texture( key1_normal, UV2 ).xyz * 		k1w;
	totalw += 									k1w;
	v += texture( key2_vertex, UV2 ).xyz * 		k2w;
	n += texture( key2_normal, UV2 ).xyz * 		k2w;
	totalw += 									k2w;
	v += texture( key3_vertex, UV2 ).xyz * 		k3w;
	n += texture( key3_normal, UV2 ).xyz * 		k3w;
	totalw += 									k3w;
	v += texture( key4_vertex, UV2 ).xyz * 		k4w;
	n += texture( key4_normal, UV2 ).xyz * 		k4w;
	totalw += 									k4w;
	
	if (totalw > 0.0) {
		VERTEX += v * min(1.0, totalw);
		NORMAL = mix( NORMAL, 		normalize(n),	min(1.0, totalw) );
	}
	
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
}
