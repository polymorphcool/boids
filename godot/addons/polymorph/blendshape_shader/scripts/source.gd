tool

extends MeshInstance

#export (Resource) var import = null setget set_import

var mesh_inst:MeshInstance = null

func set_import( r:Resource ):
	mesh = null
	if r != null:
		mesh_inst = null
		seek_mesh( r.instance() )
		if mesh_inst != null:
			mesh = mesh_inst.mesh

func seek_mesh( root:Spatial ):
	if root is MeshInstance:
		if root.mesh != null:
			if root.mesh.get_blend_shape_count() > 0:
				mesh_inst =  root
				return
	for c in root.get_children():
		seek_mesh( c )
