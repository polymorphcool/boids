tool

extends MeshInstance

export (float,0,20) var speed:float = 1
export (bool) var play:bool = false setget set_play

var playhead:float = 0

func set_play(b:bool):
	play = b
	if !play:
		playhead = 0
		reset_weights()

func reset_weights():
	for i in range(0,5):
		material_override.set_shader_param( 'key'+str(i)+'_weight', 0 )

func apply_weights():
	while playhead > 5:
		playhead -= 5
	if playhead <= 1:
		material_override.set_shader_param( 'key0_weight', 1-playhead )
		material_override.set_shader_param( 'key1_weight', playhead )
		material_override.set_shader_param( 'key2_weight', 0 )
		material_override.set_shader_param( 'key3_weight', 0 )
		material_override.set_shader_param( 'key4_weight', 0 )
	elif playhead <= 2:
		material_override.set_shader_param( 'key0_weight', 0 )
		material_override.set_shader_param( 'key1_weight', 2-playhead )
		material_override.set_shader_param( 'key2_weight', playhead-1 )
		material_override.set_shader_param( 'key3_weight', 0 )
		material_override.set_shader_param( 'key4_weight', 0 )
	elif playhead <= 3:
		material_override.set_shader_param( 'key0_weight', 0 )
		material_override.set_shader_param( 'key1_weight', 0 )
		material_override.set_shader_param( 'key2_weight', 3-playhead )
		material_override.set_shader_param( 'key3_weight', playhead-2 )
		material_override.set_shader_param( 'key4_weight', 0 )
	elif playhead <= 4:
		material_override.set_shader_param( 'key0_weight', 0 )
		material_override.set_shader_param( 'key1_weight', 0 )
		material_override.set_shader_param( 'key2_weight', 0 )
		material_override.set_shader_param( 'key3_weight', 4-playhead )
		material_override.set_shader_param( 'key4_weight', playhead-3 )
	else:
		material_override.set_shader_param( 'key0_weight', playhead-4 )
		material_override.set_shader_param( 'key1_weight', 0 )
		material_override.set_shader_param( 'key2_weight', 0 )
		material_override.set_shader_param( 'key3_weight', 0 )
		material_override.set_shader_param( 'key4_weight', 5-playhead )

func _process(delta):
	if !play:
		return
	playhead += speed * delta
	apply_weights()
