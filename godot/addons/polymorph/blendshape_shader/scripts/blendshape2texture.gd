tool

extends Spatial

export (String) 		var export_path:String = "res://"
export (NodePath) 		var source_meshinstance:NodePath = ""
export (NodePath) 		var source_animation:NodePath = ""
export (String) 		var source_track:String = ""
export (float,0,100) 	var anim_start:float = 0
export (float,0,100) 	var anim_end:float = 1
export (float,0,1) 		var anim_grain:float = 0.1
export (bool)			var anim_loop:bool = true
export (bool)			var anim_palindrome:bool = true
export (NodePath) 		var target_meshinstance:NodePath = ""
export (bool) 			var generate:bool = false setget set_generate

var initialised:bool = 				false

var source:MeshInstance = 			null
var animator:AnimationPlayer = 		null
var target:MeshInstance = 			null
var mesh = 							null
var blendshapes:Array = 			[]
var surface:Array = 				[]
var texsize:int = 					0

#-- mesh array accessors --#
# Mesh.ARRAY_VERTEX 		0
# Mesh.ARRAY_NORMAL			1
# Mesh.ARRAY_TANGENT		2
# Mesh.ARRAY_COLOR			3
# Mesh.ARRAY_TEX_UV			4
# Mesh.ARRAY_TEX_UV2		5
# Mesh.ARRAY_BONES			6
# Mesh.ARRAY_WEIGHTS		7
# Mesh.ARRAY_INDEX			8
# Mesh.ARRAY_MAX			9

func get_pow2( i:int ):
	var out:int = 2
	while out < i:
		out *= 2
	return out

func save_reload( path:String, obj:Resource ):
	ResourceSaver.save( path, obj )
	# regenerating the cache
	ResourceLoader.load( path, '', true )

func bake_blendshape( name, arr:Array ):
	
	print( "generate ", name )
	# storing vertices
	var vim:Image = Image.new()
	vim.create( texsize, 1, false, Image.FORMAT_RGBF )
	
	var tim:ImageTexture = ImageTexture.new()
	tim.storage = ImageTexture.STORAGE_RAW
	tim.lossy_quality = 1
	
	var original:PoolVector3Array = surface[Mesh.ARRAY_VERTEX]
	var verts:PoolVector3Array = arr[0]
	vim.lock()
	for i in range(0,verts.size()):
		var v:Vector3 = verts[i] - original[i]
		vim.set_pixel( i, 0, Color(v.x,v.y,v.z) )
	vim.unlock()
	tim.create_from_image( vim, 0 )
	if export_path != "":
		save_reload( export_path + name + '_vertex.tex', tim )
	
	var norma:PoolVector3Array = arr[1]
	vim.lock()
	for i in range(0,norma.size()):
		var v:Vector3 = norma[i]
		vim.set_pixel( i, 0, Color(v.x,v.y,v.z) )
	vim.unlock()
	tim.create_from_image( vim, 0 )
	if export_path != "":
		save_reload( export_path + name + '_normal.tex', tim )
	
	if arr.size() == 2:
		return
	
	var tangs:PoolRealArray = arr[2]
	vim.lock()
	for i in range(0,tangs.size(),4):
# warning-ignore:integer_division
		vim.set_pixel( i/4, 0, Color(tangs[i],tangs[i+1],tangs[i+2],tangs[i+3]) )
	vim.unlock()
	tim.create_from_image( vim, 0 )
	if export_path != "":
		save_reload( export_path + name + '_tangent.tex', tim )

func bake_animation():
	
	var animation:Animation = animator.get_animation( source_track )
	var tracks:Array = []
	for bs in blendshapes:
		var tid:int = animation.find_track( source.name + ":blend_shapes/" + bs )
		if tid != -1:
			tracks.append({ 'name': bs, 'tackid': tid })
	
	if tracks.empty():
		printerr( 'no naimation found for blend shapes of ', source.name )
		return
	
	var w:int = int( ( anim_end + anim_grain - anim_start) / anim_grain )
	var h:int = int(ceil(tracks.size()/3.0))
	
	var vim:Image = Image.new()
	vim.create( w, h, false, Image.FORMAT_RGBF )
	var tim:ImageTexture = ImageTexture.new()
	tim.storage = ImageTexture.STORAGE_RAW
	tim.lossy_quality = 1
	
	animator.assigned_animation = source_track
	vim.lock()
	var time:float = anim_start
	for x in range( 0, w ):
		animator.seek( time, true )
		var y:int = 0
		var channel:int = -1
		var c:Color = Color(0,0,0)
		for t in range( 0, tracks.size() ):
			channel += 1
			var f:float = source.get("blend_shapes/"+tracks[t].name)
			match channel:
				0:
					c.r = f
				1:
					c.g = f
				2:
					c.b = f
			if channel == 2:
				vim.set_pixel( x, y, c )
				y += 1
				c = Color(0,0,0)
				channel = -1
		# pushing last color
		if channel != -1:
			vim.set_pixel( x, y, c )
		time += anim_grain
	vim.unlock()
	
	var tflag:int = 0
	if anim_loop:
		tflag = Texture.FLAG_REPEAT
	if anim_palindrome:
		tflag = Texture.FLAG_MIRRORED_REPEAT
	tim.create_from_image( vim, tflag )
	if export_path != "":
		save_reload( export_path + source_track + '_anim.tex', tim )


func generate_mesh():
	
	var offx:float = .5 / texsize
	# storing adapted uvs in UV2
	surface[Mesh.ARRAY_TEX_UV2] = []
	for i in range(0,surface[Mesh.ARRAY_VERTEX].size()):
		surface[Mesh.ARRAY_TEX_UV2].append( Vector2( offx + i * 1.0 / texsize, 0.5 ) )
	var st:SurfaceTool = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	# copying the mesh
	for i in range(0,surface[Mesh.ARRAY_VERTEX].size()):
		for a in range(0,Mesh.ARRAY_MAX):
			if surface[a] == null:
				continue
			match a:
				Mesh.ARRAY_NORMAL:
					st.add_normal( surface[a][i] )
				Mesh.ARRAY_TANGENT:
					var index:int = int(i*4)
					var p:Plane = Plane( surface[a][index], surface[a][index+1], surface[a][index+2], surface[a][index+3] )
					st.add_tangent( p )
				Mesh.ARRAY_COLOR:
					st.add_color( surface[a][i] )
				Mesh.ARRAY_TEX_UV:
					st.add_uv( surface[a][i] )
				Mesh.ARRAY_TEX_UV2:
					st.add_uv2( surface[a][i] )
				Mesh.ARRAY_BONES:
					var index:int = int(i*4)
					var pia:PoolIntArray = [ surface[a][index], surface[a][index+1], surface[a][index+2], surface[a][index+3] ]
					st.add_bones( pia )
				Mesh.ARRAY_WEIGHTS:
					var index:int = int(i*4)
					var pra:PoolRealArray = [ surface[a][index], surface[a][index+1], surface[a][index+2], surface[a][index+3] ]
					st.add_weights( pra )
		st.add_vertex( surface[Mesh.ARRAY_VERTEX][i] )
	
	for i in surface[Mesh.ARRAY_INDEX]:
		st.add_index( i )
	var newm:Mesh = Mesh.new()
	st.commit( newm )
	if export_path != "":
		save_reload( export_path + source.name + '.mesh', newm )
	if target != null:
		target.mesh = newm

func set_generate(b:bool):
	
	generate = false
	
	if b:
		
		source = get_node( source_meshinstance )
		if !source is  MeshInstance:
			source = null
			return
		mesh = source.mesh
		if mesh == null:
			return
		
		animator = get_node( source_animation )
		if !animator is AnimationPlayer:
			animator = null
		else:
			var pas:PoolStringArray = animator.get_animation_list()
			# getting track
			var found:bool = false
			for i in pas:
				if i == source_track:
					found = true
			if !found:
				source_track = ""
			if source_track == "":
				if pas.size() > 0:
					source_track = pas[0]
			if source_track == "":
				print( 'could not load any animator...' )
				animator = null
		
		target = get_node( target_meshinstance )
		if !target is MeshInstance:
			target = null
		else:
			target.mesh = null
		
		blendshapes = []
		for i in range(0,mesh.get_blend_shape_count()):
			var bs:String = mesh.get_blend_shape_name(i)
			blendshapes.append( bs )
		
		surface = mesh.surface_get_arrays(0)
		var vnum:int = surface[Mesh.ARRAY_VERTEX].size()
		
		texsize = get_pow2(vnum)
		print( "vertex texture: ", texsize )
		
		var bs:Array = mesh.surface_get_blend_shape_arrays(0)
		for i in range(0,bs.size()):
			var n:String = blendshapes[i]
			var bsv:PoolVector3Array = 	bs[i][Mesh.ARRAY_VERTEX]
			var bsn:PoolVector3Array = 	bs[i][Mesh.ARRAY_NORMAL]
			var bst:PoolRealArray = 	bs[i][Mesh.ARRAY_TANGENT]
			var arr:Array = []
			if  bsv == null or bsv.size() != vnum:
				print( "failed to find vertices in ", n )
				continue
			if  bsn == null or bsn.size() != vnum:
				print( "failed to find normals in ", n )
				continue
			# create texture slot
			arr.append( bsv )
			arr.append( bsn )
			if bst != null and bst.size() == vnum * 4:
				arr.append( bst )
			bake_blendshape( n, arr )
		
		if animator != null:
			bake_animation()
		
		generate_mesh()

# warning-ignore:unused_argument
func _process(delta):
	if !initialised:
#		set_generate(true)
		initialised = true
