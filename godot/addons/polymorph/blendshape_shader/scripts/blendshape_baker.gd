tool

extends Spatial

export (Resource) 			var import = null setget set_import
export (String) 			var export_path:String = "res://addons/polymorph/blendshape_shader/exports/"
export (bool) 				var optimise:bool = false
export (bool) 				var bake:bool = false setget set_bake

var initialised:bool = false
var mesh = null
var surface:Array = []
var vert_index:Array = []
var unique_vertices:Array = []
var bs_packs:Array = []
var tex_width_max:int = 2048
var bake_width:int = 0

func set_import( r:Resource ):
	import = r
	$source.set_import(r)

func get_shader():
	var s:Shader = Shader.new()
	s.code = ""
	s.code += "shader_type spatial;\n"
	s.code += "render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;\n"
	s.code += "uniform vec4 albedo = vec4(1.0);\n"
	s.code += "uniform sampler2D texture_albedo : hint_albedo;\n"
	s.code += "uniform float specular = 0.5;\n"
	s.code += "uniform float metallic = 0.0;\n"
	s.code += "uniform float roughness = 1.0;\n"
	s.code += "uniform float point_size : hint_range(0,128);\n"
	s.code += "uniform vec3 uv1_scale;\n"
	s.code += "uniform vec3 uv1_offset;\n"
	s.code += "uniform vec3 uv2_scale;\n"
	s.code += "uniform vec3 uv2_offset;\n"
	s.code += "uniform sampler2D animation: hint_black;\n"
	s.code += "uniform float animation_weight = 1.0;\n"
	s.code += "uniform float animation_line = 1.0;\n"
	s.code += "uniform float animation_frames = 1.0;\n"
	s.code += "uniform float animation_frame_gap = 1.0;\n"
#	s.code += "uniform float time = 0.;\n"
	s.code += "// getting frame indices\n"
	s.code += "void compute_frames( float t, float gap, float lines, float frame_count, inout float fprev, inout float fnext, inout float fmix, inout float ratio ) {\n"
	s.code += "	float anim_frame = (t/gap) * frame_count;\n"
	s.code += "	fprev = float(int(anim_frame));\n"
	s.code += "	fnext = fprev + 1.0;\n"
	s.code += "	fmix = anim_frame-fprev;"
	s.code += "	fprev *= lines / frame_count;\n" 
	s.code += "	fnext *= lines / frame_count;\n"
	s.code += "	ratio = float(frame_count * lines) * 2.;\n"
	s.code += "}\n"
	s.code += "// scaling UV2 y to match animation\n"
	s.code += "vec2 compute_uv( vec2 uv, float ratio ) {\n"
	s.code += "	vec2 anim_uv = uv;\n"
	s.code += "	anim_uv.y /= ratio;\n"
	s.code += "	anim_uv.y += 0.5 / ratio;\n"
	s.code += "	return anim_uv\n;"
	s.code += "}\n"
	s.code += "// getting the frames for vertex or normal, depending on uv\n"
	s.code += "vec3 extract_vec3( sampler2D tex, vec2 uv, float fprev, float fnext, float fmix ) {\n"
	s.code += "	vec3 v_prev = texture( tex, uv + vec2(0., fprev ) ).xyz;\n"
	s.code += "	vec3 v_next = texture( tex, uv + vec2(0., fnext ) ).xyz;\n"
	s.code += "	return mix( v_prev, v_next, fmix );\n"
	s.code += "}\n"
	s.code += "void vertex() {\n"
	s.code += "	UV=UV*uv1_scale.xy+uv1_offset.xy;\n"
	s.code += "	vec3 origin_vertex = VERTEX;\n"
	s.code += "	/* animation specific process -- starts */\n"
	s.code += "	float frame_prev;\n"
	s.code += "	float frame_next;\n"
	s.code += "	float frame_mix;\n"
	s.code += "	float yratio;\n"
	s.code += "	compute_frames( TIME, animation_frame_gap, animation_line, animation_frames, frame_prev, frame_next, frame_mix, yratio );\n"
	s.code += "	vec2 anim_uv = compute_uv( UV2, yratio );\n"
	s.code += "	vec3 v_mix = extract_vec3( animation, anim_uv, frame_prev, frame_next, frame_mix );\n"
	s.code += "	vec3 n_mix = extract_vec3( animation, anim_uv + vec2(0., animation_line/yratio), frame_prev, frame_next, frame_mix );\n"
	s.code += "	VERTEX += v_mix * animation_weight;\n"
	s.code += "	NORMAL = mix(NORMAL, n_mix, animation_weight);\n"
	s.code += "	/* animation specific process -- ends */\n"
	s.code += "}\n"
	s.code += "void fragment() {\n"
	s.code += "	vec2 base_uv = UV;\n"
	s.code += "	vec4 albedo_tex = texture(texture_albedo,base_uv);\n"
	s.code += "	ALBEDO = albedo.rgb * albedo_tex.rgb;\n"
	s.code += "	METALLIC = metallic;\n"
	s.code += "	ROUGHNESS = roughness;\n"
	s.code += "	SPECULAR = specular;\n"
	s.code += "}\n"
	return s

func get_pow2( i:int ):
	var out:int = 2
	while out < i:
		out *= 2
	return out

func save_reload( path:String, obj:Resource ):
	ResourceSaver.save( path, obj )
	# regenerating the cache
	ResourceLoader.load( path, '', true )

func generate_mesh():
	
	var offx:float = .5 / bake_width
	# storing adapted uvs in UV2
	surface[Mesh.ARRAY_TEX_UV2] = []
	for i in range(0,surface[Mesh.ARRAY_VERTEX].size()):
		var x:int = vert_index[i]
		var y:int = 0
		if x >= bake_width:
			y = x / bake_width
			x -= y * bake_width
		surface[Mesh.ARRAY_TEX_UV2].append( Vector2( offx + x * 1.0 / bake_width, y ) )
		
	var st:SurfaceTool = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	# copying the mesh
	for i in range(0,surface[Mesh.ARRAY_VERTEX].size()):
		for a in range(0,Mesh.ARRAY_MAX):
			if surface[a] == null:
				continue
			match a:
				Mesh.ARRAY_NORMAL:
					st.add_normal( surface[a][i] )
				Mesh.ARRAY_TANGENT:
					var index:int = int(i*4)
					var p:Plane = Plane( surface[a][index], surface[a][index+1], surface[a][index+2], surface[a][index+3] )
					st.add_tangent( p )
				Mesh.ARRAY_COLOR:
					st.add_color( surface[a][i] )
				Mesh.ARRAY_TEX_UV:
					st.add_uv( surface[a][i] )
				Mesh.ARRAY_TEX_UV2:
					st.add_uv2( surface[a][i] )
				Mesh.ARRAY_BONES:
					var index:int = int(i*4)
					var pia:PoolIntArray = [ surface[a][index], surface[a][index+1], surface[a][index+2], surface[a][index+3] ]
					st.add_bones( pia )
				Mesh.ARRAY_WEIGHTS:
					var index:int = int(i*4)
					var pra:PoolRealArray = [ surface[a][index], surface[a][index+1], surface[a][index+2], surface[a][index+3] ]
					st.add_weights( pra )
		st.add_vertex( surface[Mesh.ARRAY_VERTEX][i] )
	
	for i in surface[Mesh.ARRAY_INDEX]:
		st.add_index( i )
	var newm:Mesh = Mesh.new()
	st.commit( newm )
	if export_path != "":
		save_reload( export_path + mesh.resource_name + '.mesh', newm )
	$destination.mesh = newm

func add_vertex( v:Vector3, n:Vector3 ):
	var vi:int = -1
	if optimise:
		var i:int = 0
		for ve in unique_vertices:
			if ve.vertex == v and ve.normal == n:
				vi = i
				break
			i += 1
	if vi == -1:
		vert_index.append( unique_vertices.size() )
		unique_vertices.append( { 'index': vert_index.size()-1, 'vertex': v, 'normal': n } )
	else:
		vert_index.append( vi )

func new_bs_pack():
	return {
		'name': '',		#name of the blendshape pack
		'list': [], 	#list of blendshape names, used to sort blendsahpes alphabetically
		'lines': 2,		#how many lines to store one frame
		'texture': null
	}

func get_bs_id( name:String ):
	for i in range(0,mesh.get_blend_shape_count()):
		var bn:String = mesh.get_blend_shape_name(i) 
		if name == bn:
			return i
	return -1

func bake_bs_pack( bs:Dictionary ):
	
	print( "bake_bs_pack\n\t", bs.name )
	print( "\tentries: ", bs.list.size() )
	var tw:int = get_pow2( unique_vertices.size() )
	# line 0 is vertex, line 1 is normals
	if tw > tex_width_max:
		bs.lines = int(ceil(tw*1.0/tex_width_max)) * 2
		tw = tex_width_max
	# storing width for mesh generation
	bake_width = tw
	# room to store vertices & normals
	var th:int = bs.list.size() * bs.lines
	
	print( "\ttexture: ", tw, 'x', th )
	
	var vim:Image = Image.new()
	vim.create( tw, th, false, Image.FORMAT_RGBF )
	var tim:ImageTexture = ImageTexture.new()
	tim.storage = ImageTexture.STORAGE_RAW
	tim.flags = Texture.FLAG_REPEAT
	tim.lossy_quality = 1
	
	var original:PoolVector3Array = surface[Mesh.ARRAY_VERTEX]
	var bs_arr:Array = mesh.surface_get_blend_shape_arrays(0)
	var y_offset:int = 0
	
	vim.lock()
	for bname in bs.list:
		print( "\tbaking ", bname, ' at ', y_offset )
		var id:int = get_bs_id( bname )
		if id == -1:
			printerr( "blendshape ", bname, " not found!" )
			continue
		var bsv:PoolVector3Array = 	bs_arr[id][Mesh.ARRAY_VERTEX]
		var bsn:PoolVector3Array = 	bs_arr[id][Mesh.ARRAY_NORMAL]
#		var tangs:PoolRealArray = 	bs_arr[id][Mesh.ARRAY_TANGENT]
		var x:int = 0
		var y:int = 0
		for ve in unique_vertices:
			var i:int = ve.index
			var v:Vector3 = bsv[i] - original[i]
			var n:Vector3 = bsn[i]
			vim.set_pixel( x, y_offset + y, Color(v.x,v.y,v.z) )
			vim.set_pixel( x, y_offset + y + bs.lines/2, Color(n.x,n.y,n.z) )
			x += 1
			if x >= tex_width_max:
				x = 0
				y += 1
		y_offset += bs.lines
	vim.unlock()
	
	tim.create_from_image( vim, 0 )
	if export_path != "":
		save_reload( export_path + mesh.resource_name + '_' + bs.name + '.tex', tim )
	
	bs.texture = tim
	bs.texture.flags = Texture.FLAG_REPEAT

func set_bake(b:bool):
	
	bake = false
	if !initialised:
		return
	
	print( "###### BAKE START ######" )
	
	mesh = $source.mesh
	if mesh == null:
		return
		
	$destination.mesh = null
	
	surface = mesh.surface_get_arrays(0)
	vert_index = []
	unique_vertices = []
	var vnum:int = surface[Mesh.ARRAY_VERTEX].size()
	for i in range( 0, vnum ):
		add_vertex(surface[Mesh.ARRAY_VERTEX][i],surface[Mesh.ARRAY_NORMAL][i])
	
	print( "total vertices: ", vert_index.size() )
	print( "optimised vertices: ", unique_vertices.size() )
	
	# packing bs_packs based on their prefix (characters before last '_')
	bs_packs = []
	var curr_prefix:String = ''
	var bss:Dictionary = {}
	for i in range(0,mesh.get_blend_shape_count()):
		var bn:String = mesh.get_blend_shape_name(i)
		var prefix:String = bn.substr( 0, bn.find_last('_') )
		if curr_prefix == '' or curr_prefix != prefix:
			if not bss.empty():
				bss.list.sort()
				bs_packs.append( bss )
			bss = new_bs_pack()
			bss.name = prefix
			bss.list.append( bn )
			curr_prefix = prefix
		else:
			bss.list.append( bn )
	if not bss.empty():
		bss.list.sort()
		bs_packs.append( bss )
	
	for bs in bs_packs:
		bake_bs_pack( bs )
	
	generate_mesh()
	
	while $previews.get_child_count() > 0:
		$previews.remove_child( $previews.get_child(0) )
	
	var grid_size = ceil( sqrt( len(bs_packs) ) )
#	var preview_offset:Vector3 = Vector3.FORWARD * 3 + Vector3.LEFT * 3 * (bs_packs.size()-1) * .5
	var preview_offset:Vector3 = Vector3(-.5,0,-.5) * (grid_size-1) * 3
	print( len(bs_packs), ' >> ', grid_size, ' >> ', preview_offset )
	var display_anim_mat:SpatialMaterial = $preview.get_child(0).material_override.duplicate()
	var x = 0
	var y = 0
	for bs in bs_packs:
		var preview_mat:ShaderMaterial = ShaderMaterial.new()
		preview_mat.shader = get_shader()
		preview_mat.set_shader_param( "animation", bs.texture )
		preview_mat.set_shader_param( "animation_line", bs.lines/2 )
		preview_mat.set_shader_param( "animation_frames", len(bs.list) )
		preview_mat.set_shader_param( "animation_frame_gap", 3 )
		var preview:MeshInstance = $preview.duplicate()
		preview.name = bs.name
		preview.mesh = $destination.mesh
		preview.material_override = preview_mat
		$previews.add_child( preview )
		preview.owner = $previews.owner
		preview.visible = true
		var anim:MeshInstance = preview.get_child(0)
		anim.material_override = display_anim_mat.duplicate()
		anim.material_override.albedo_texture = bs.texture
		var ts:Vector2 = bs.texture.get_size()
		var tratio = ts.y / ts.x
		anim.scale.z = tratio * 2
		anim.translation.y = tratio * 2 + 0.1
		preview.translation = preview_offset + Vector3(x,0,y)*3
		x += 1
		if x >= grid_size:
			x = 0
			y += 1

# warning-ignore:unused_argument
func _process(delta):
	if !initialised:
		initialised = true
