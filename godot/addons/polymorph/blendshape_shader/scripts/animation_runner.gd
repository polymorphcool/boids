tool

extends MeshInstance

export (float,0,10) var speed:float = 1
export (bool) var play:bool = true

var time:float = 0 

func _process(delta):
	
	if !play:
		return
	
	time += delta * speed
	material_override.set_shader_param( "time", time )
	
