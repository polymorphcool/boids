tool

extends Spatial

var initialised:bool = false
var baction:BoidAction = BoidAction.new()

func boid_event( boid_id:int, event_id:int ):
#	print( "boid_event: ", boid_id, " > ",  event_id )
#	$BoidSystem.apply_action(boid_id, baction)

	$BoidSystem.set_boid_attribute( boid_id, Boid.BACTION_ATTR_ACTIVE, false )
#	$BoidSystem.set_boid_attribute( boid_id, Boid.BACTION_ATTR_COLOR, Color(1,0,0) )
	pass

func _process(delta):
	if !initialised:
		baction.set_type(Boid.BACTION_TYPE_SET_ATTRIBUTE)
		baction.set_attribute(Boid.BACTION_ATTR_ACTIVE)
		baction.set_bool(false)
		$BoidSystem.connect("boid_event",self,"boid_event")
		initialised = true
