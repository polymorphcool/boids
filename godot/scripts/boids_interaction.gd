tool

extends Boids

export (bool) var restart:bool = false

#export (int,0,5000) var from:int = 0 
#export (int,0,5000) var to:int = 5000
#export (bool) var move:bool = false setget set_move
#export (bool) var dir:bool = false setget set_dir
#export (bool) var front:bool = false setget set_front

var initialised:bool = false

#func set_move( b:bool ):
#	move = false
#	if b and initialised:
#		set_boids_position( Vector3.ZERO, from, to)
#
#func set_dir( b:bool ):
#	move = false
#	if b and initialised:
#		set_boids_direction( Vector3(0,0,1), from, to)
#
#func set_front( b:bool ):
#	front = false
#	if b and initialised:
#		set_boids_forward( Vector3(0,0,1), from, to)

func _process(delta):
	initialised = true
	if restart:
		restart()
		restart = false
