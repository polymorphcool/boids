tool

extends Spatial

export (int,1,50) var domain_x:int = 4
export (int,1,50) var domain_y:int = 4
export (int,1,50) var domain_z:int = 4

export (bool) var play:bool = false

var initialised:bool = false
var grid:Array = []

func vmin( a:Vector3, b:Vector3 ):
	return Vector3( min(a.x,b.x), min(a.y,b.y), min(a.z,b.z) )

func vmax( a:Vector3, b:Vector3 ):
	return Vector3( max(a.x,b.x), max(a.y,b.y), max(a.z,b.z) )

func box_size( s:Spatial ):
	var _min:Vector3 = Vector3.ZERO
	var _max:Vector3 = Vector3.ZERO
	var p:Vector3 = s.global_transform.basis.xform(Vector3(1,1,1))
	_min = vmin(p,_min)
	_max = vmax(p,_max)
	p = s.global_transform.basis.xform(Vector3(1,1,1))
	_min = vmin(p,_min)
	_max = vmax(p,_max)
	p = s.global_transform.basis.xform(Vector3(-1,1,1))
	_min = vmin(p,_min)
	_max = vmax(p,_max)
	p = s.global_transform.basis.xform(Vector3(1,-1,1))
	_min = vmin(p,_min)
	_max = vmax(p,_max)
	p = s.global_transform.basis.xform(Vector3(-1,-1,1))
	_min = vmin(p,_min)
	_max = vmax(p,_max)
	return  vmax( (_min*-1) , _max ) * 2

func display_cells( pos:Vector3, size:Vector3 ):
	var p:Vector3 = pos + Vector3(domain_x,domain_y,domain_z)*.5
	for z in range( int(p.z-size.z), int(p.z+size.z)+1 ):
		if z < 0 or z >= domain_z:
			continue
		for y in range( int(p.y-size.y), int(p.y+size.y)+1 ):
			if y < 0 or y >= domain_y:
				continue
			for x in range( int(p.x-size.x), int(p.x+size.x)+1 ):
				if x < 0 or x >= domain_x:
					continue
				var id:int = x + y * domain_x + z * domain_x * domain_y
				if id > grid.size():
					continue
				grid[id].visible = true

func clear_grid():
	grid = []
	while $grid.get_child_count() > 0:
		$grid.remove_child( $grid.get_child(0) )

func regenerate_grid():
	clear_grid()
	var off:Vector3 = -Vector3(domain_x,domain_y,domain_z)*.5
	var cnum:int = domain_x*domain_y*domain_z
	for z in range( 0, domain_z ):
		for y in range( 0, domain_y ):
			for x in range( 0, domain_x ):
				var c = $cell.duplicate()
				$grid.add_child(c)
				c.visible = false
				c.translation = off + Vector3(x,y,z) + Vector3.ONE * .5
				grid.append(c)

func reset_grid():
	for c in grid:
		c.visible = false

func _process(delta):
	
	if !play:
		clear_grid()
		return
	
	if !initialised:
		initialised = true
	
	while $tmp.get_child_count() > 0:
		$tmp.remove_child( $tmp.get_child(0) )
	
	var a_center:Vector3 = $A.global_transform.origin
	var b_center:Vector3 = $B.global_transform.origin
	
	var self_aabb:MeshInstance = $aabb.duplicate()
	$tmp.add_child(self_aabb)
	self_aabb.visible = true
	self_aabb.global_transform.origin = Vector3.ZERO
	self_aabb.global_transform.basis = self_aabb.global_transform.basis.scaled( Vector3( domain_x, domain_y, domain_z ) * .5 )
	
	var a_outter:Vector3 = box_size($A)
	var a_aabb:MeshInstance = $aabb.duplicate()
	$tmp.add_child(a_aabb)
	a_aabb.visible = true
	a_aabb.global_transform.origin = a_center
	a_aabb.global_transform.basis = a_aabb.global_transform.basis.scaled( a_outter * .5 )
	
	var b_outter:Vector3 = box_size($B)
	var b_aabb:MeshInstance = $aabb.duplicate()
	$tmp.add_child(b_aabb)
	b_aabb.visible = true
	b_aabb.global_transform.origin = b_center
	b_aabb.global_transform.basis = b_aabb.global_transform.basis.scaled( b_outter * .5 )
	
	# display 
	var cnum:int = domain_x*domain_y*domain_z
	if cnum != grid.size():
		regenerate_grid()
	reset_grid()
	
	display_cells( a_center, a_outter * .5 )
	display_cells( b_center, b_outter * .5 )
