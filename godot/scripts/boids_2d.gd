tool

extends Node2D

export (int,0,10000) 	var number:int = 100
export (int,0,1000) 	var cell_size:int = 100
export (Vector2) 		var boundary:Vector2 = Vector2(500,500)
export (bool) 			var generate:bool = false 		setget do_generate
export (bool) 			var run:bool = false 
export (bool) 			var display_grid:bool = false

export (float,0,200) 	var speed:float = 2 			setget set_speed
export (float,0,1) 		var separation:float = 0.5 		setget set_separation
export (float,0,1) 		var alignment:float = 0.5 		setget set_alignment
export (float,0,1) 		var cohesion:float = 0.5 		setget set_cohesion
export (float,0,1) 		var sociability:float = 0.99 	setget set_sociability
export (float,0,80) 	var reactivity:float = 5	 	setget set_reactivity

export (float,0,80) 	var boundary_strength:float = 5

var initialised:bool = false
var offset:Vector2 = Vector2.ZERO
var cell:Line2D = null
var grid_offsetx:int = 0
var grid_offsety:int = 0
var grid_rows:int = 0
var grid_cols:int = 0
var grid_size:int = 0
var grid:Array = []

func set_separation(f:float):
	separation = f

func set_alignment(f:float):
	alignment = f

func set_cohesion(f:float):
	cohesion = f

func set_speed(f:float):
	speed = f

func set_sociability(f:float):
	sociability = f

func set_reactivity(f:float):
	reactivity = f

func do_generate(b:bool):
	generate = false
	if b:
		prepare()

func vp_resize():
	offset = get_viewport_rect().size * .5
	$swarm.position = offset

func prepare():
	
	while $swarm.get_child_count() > 0:
		var c:Node = $swarm.get_child(0)
		$swarm.remove_child( c )
		c.queue_free()
	
	while $grid.get_child_count() > 0:
		var c:Node = $grid.get_child(0)
		$grid.remove_child( c )
		c.queue_free()
	
	$line.clear_points()
	
	
	var tmpl:Node2D = $boid
	tmpl.visible = false
# warning-ignore:unused_variable
	for i in range(0,number):
		var b:Node2D = tmpl.duplicate()
		b.manager = self
		$swarm.add_child( b )
		b.visible = true
		b.position = boundary * Vector2(rand_range(-1,1),rand_range(-1,1))
		b.direct_to( Vector2.UP.rotated(rand_range(0,TAU)).normalized() )
	
	cell = Line2D.new()
	cell.width = 4
	cell.default_color = Color(1,0,0,1)
	cell.add_point( Vector2( 0,0 ) )
	cell.add_point( Vector2( cell_size,0 ) )
	cell.add_point( Vector2( cell_size,cell_size ) )
	cell.add_point( Vector2( 0,cell_size ) )
	cell.add_point( Vector2( 0,0 ) )
	var lbl:Label = Label.new()
	cell.add_child( lbl )
	lbl.rect_position = Vector2.ONE * cell_size * .3

func _ready():
	if !Engine.editor_hint:
		vp_resize()
		prepare()
		get_viewport().connect("size_changed",self,"vp_resize")
		run = true
		display_grid = false

func _input(event):
	if Engine.editor_hint:
		return
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_SPACE:
			prepare()
		elif event.scancode == KEY_ESCAPE:
			get_tree().quit()

func get_cell( x:int, y:int ):
	if x < grid_offsetx or x >= grid_offsetx + grid_rows:
		return []
	elif y < grid_offsety or y >= grid_offsety + grid_cols:
		return []
	else:
		return grid[ (x-grid_offsetx) + (y-grid_offsety) * grid_rows ]

func get_neighbours( boid:Node2D ):
	boid.neighbours = []
	if boid.cellid == -1:
		return
	for y in range( boid.gridy-1, boid.gridy+2 ):
		for x in range( boid.gridx-1, boid.gridx+2 ):
			boid.neighbours += get_cell(x,y)

func _process(delta):
	
	if !run:
		return
	
	$fps.text = str(Engine.get_frames_per_second())
	
	var bb_min:Vector2 = offset
	var bb_max:Vector2 = offset
	for b in $swarm.get_children():
		get_neighbours( b )
		b.move( delta )
		if bb_min.x > b.position.x:
			bb_min.x = b.position.x
		if bb_min.y > b.position.y:
			bb_min.y = b.position.y
		if bb_max.x < b.position.x:
			bb_max.x = b.position.x
		if bb_max.y < b.position.y:
			bb_max.y = b.position.y
	
	
	var minx:int = int(floor(bb_min.x/cell_size))
	var miny:int = int(floor(bb_min.y/cell_size))
	var maxx:int = int(ceil(bb_max.x/cell_size))
	var maxy:int = int(ceil(bb_max.y/cell_size))
	
	grid_offsetx = minx
	grid_offsety = miny
	grid_rows = (maxx-minx)
	grid_cols = (maxy-miny)
	grid_size = grid_rows * grid_cols
	
	# grid resizing
	if grid.size() > grid_size:
		grid.resize( grid_size )
#		print( "resizing grid to ", str(total) )
	elif grid.size() < grid_size:
		while grid.size() < grid_size:
			grid.append([])
#		print( "resizing grid to ", str(total) )
	# grid cleanup
	for i in range(0,grid_size):
		grid[i] = []
	# grid registration
	for b in $swarm.get_children():
		b.cellid = (b.gridx-grid_offsetx) + (b.gridy-grid_offsety) * grid_rows
		if b.cellid >= grid_size:
			print( "too far!!! ", b.cellid, "/", grid_size )
		else:
			grid[b.cellid].append(b)
	
	# link to center of the current cell
#	var cell_center:Vector2 = Vector2.ONE * cell_size * .5
#	for b in $swarm.get_children():
#		var cp:Vector2 = Vector2( b.gridx, b.gridy ) * cell_size
#		cp += cell_center
#		cp += offset
#		b.get_child(1).set_point_position( 1, b.to_local(cp) )
	
	if !display_grid or cell == null:
		return
	
	$line.clear_points()
	$line.position = $swarm.position
	$line.add_point( Vector2( bb_min.x, bb_min.y ) )
	$line.add_point( Vector2( bb_max.x, bb_min.y ) )
	$line.add_point( Vector2( bb_max.x, bb_max.y ) )
	$line.add_point( Vector2( bb_min.x, bb_max.y ) )
	$line.add_point( Vector2( bb_min.x, bb_min.y ) )
	
	$grid.position = $swarm.position
	while $grid.get_child_count() > 0:
		var c:Node = $grid.get_child(0)
		$grid.remove_child( c )
		c.queue_free()
	for y in range( miny, maxy ):
		for x in range( minx, maxx ):
			var c:Line2D = cell.duplicate()
			$grid.add_child( c )
			c.position = Vector2( x * cell_size, y * cell_size )
			var id:int = (x-grid_offsetx) + (y-grid_offsety) * grid_rows
			c.get_child(0).text = str( grid[id].size() )
