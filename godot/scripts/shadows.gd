extends CheckBox

onready var light:Light = get_node( "../../cam_pivot/cam/sun" )

func _ready():
	pressed = light.shadow_enabled
	self.connect("pressed",self,"adapt_shadows")

func adapt_shadows():
	light.shadow_enabled = pressed
