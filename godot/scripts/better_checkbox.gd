extends CheckBox

signal changed_pressed

func _ready():
	self.connect( "pressed", self, "emit_pressed" )

func emit_pressed():
	emit_signal("changed_pressed", self, self.pressed )
