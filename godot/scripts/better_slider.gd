extends HSlider

signal changed_better

func _ready():
	self.connect( "value_changed", self, "emit_changed" )

func emit_changed(f:float):
	emit_signal("changed_better", self, f)
