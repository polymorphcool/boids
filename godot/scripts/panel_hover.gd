extends PanelContainer

onready var style = get("custom_styles/panel")
onready var default_color = get("custom_styles/panel").bg_color

onready var alpha_target:float = default_color.a
onready var alpha_current:float = default_color.a

func _ready():
	mouse_out()
	connect( "mouse_entered", self, "mouse_in" )
	connect( "mouse_exited", self, "mouse_out" )

func mouse_in():
	alpha_target = default_color.a

func mouse_out():
	alpha_target = 0

func _process(delta):
	
	if alpha_current == alpha_target:
		return
	alpha_current += (alpha_target-alpha_current) * 5 * delta
	if abs(alpha_target-alpha_current) < 1e-4:
		alpha_current = alpha_target
	get("custom_styles/panel").bg_color = Color(default_color.r,default_color.g,default_color.b,alpha_current)
