tool

extends Spatial

export (int,1,200) var max_neighbours:int = 20
export (float,0,20) var radius:float = 1
export (float,0,1) var speed_randomness:float = 0.01

var direction:Vector3 = Vector3.FORWARD
var push:Vector3 = Vector3.ZERO
var last_direction:Vector3 = Vector3.FORWARD
var dir_norm:bool = true

var manager:Spatial = null
var gridx:int = 0
var gridy:int = 0
var gridz:int = 0
var cellid:int = -1
var neighbours:Array = []
var curr_speed:float = 1
var speed_mult:float = 1
var separation_vec:Vector3 = Vector3.ZERO
var alignment_vec:Vector3 = Vector3.ZERO
var cohesion_vec:Vector3 = Vector3.ZERO

func _ready():
	speed_mult = rand_range(1-speed_randomness,1)

func direct_to( v:Vector3 ):
	direction = v
	last_direction = v

func move(delta:float):
	
	if manager != null:
		curr_speed += ( manager.speed * speed_mult - curr_speed ) * 15 * delta
	
	translation += direction * curr_speed * delta + push * delta
	push -= push * delta
	
	if manager != null:
		direction -= direction * manager.direction_flattening * delta
		direction += manager.constant_force * delta
		dir_norm = true
		
		if translation.x <= -manager.boundary.x:
			direction += Vector3(1,0,0) * manager.boundary_strength * delta
		elif translation.x >= manager.boundary.x:
			direction += Vector3(-1,0,0) * manager.boundary_strength * delta
		if translation.y <= -manager.boundary.y:
			direction += Vector3(0,1,0) * manager.boundary_strength * delta
		elif translation.y >= manager.boundary.y:
			direction += Vector3(0,-1,0) * manager.boundary_strength * delta
		if translation.z <= -manager.boundary.z:
			direction += Vector3(0,0,1) * manager.boundary_strength * delta
		elif translation.z >= manager.boundary.z:
			direction += Vector3(0,0,-1) * manager.boundary_strength * delta
			
	
	var nnum:int = neighbours.size() - 1 # there is always one neighbour: self
	if manager != null and nnum > 0:
		var naccurate:int = 0
		var dmutl = manager.reactivity * delta
		separation_vec = Vector3.ZERO
		alignment_vec = Vector3.ZERO
		cohesion_vec = Vector3.ZERO
		var nsize:int = neighbours.size()
		var num:int = nsize
		var start:int = 0
		if num > max_neighbours:
			start = int(rand_range(0,num))
			num = max_neighbours
		for i in range(start,start+num):
			var n:Spatial = neighbours[i%nsize]
			if n == self:
				continue
			var dist:Vector3 = translation - n.translation
			var l:float = dist.length()
			var lratio:float = 1-(l/manager.cell_size)
			if l < radius + n.radius:
				var npush:Vector3 = dist.normalized()
				var ddir:float = 1 - abs(last_direction.dot(npush))
				var ndir:float = (1 + last_direction.dot(n.last_direction))*.5
				npush *= 50 * ddir * delta
				push += npush * radius
				n.push -= npush * n.radius
			if l <= manager.cell_size:
				separation_vec += dist * lratio
				alignment_vec += n.direction * manager.cell_size * lratio
				cohesion_vec += n.translation
				naccurate += 1
		if naccurate > 0:
			separation_vec /= naccurate * max(0.01, manager.sociability)
			alignment_vec /= naccurate
			cohesion_vec /= naccurate
			direction += separation_vec * 				manager.separation * dmutl
			direction += alignment_vec * 				manager.alignment * dmutl
			direction += (cohesion_vec-translation) * 	manager.cohesion * dmutl
			dir_norm = true
#		var diff:Vector2 = n.position - position
#		var dsqrt:float = diff.length()
#		# pushing each other
#		if dsqrt < 10:
#			position -= diff * 30 * delta
#		# avoiding behaviour
#		if dsqrt < 200:
#			var m:float = last_direction.dot( n.last_direction )
#			m = 1 - ( (1+m)*.5 )
#			var d:float = (1-(dsqrt/200))
#			direction -= diff.normalized() * (2+m*8) * d * delta 
#			curr_speed += -curr_speed * m * d * delta
#		dir_norm = true
	
	if dir_norm:
		direction = direction.normalized()
		var right:Vector3 = Vector3.UP.cross( direction ).normalized()
		var up:Vector3 = direction.cross( right ).normalized()
		transform.basis = Basis( right, up, direction )
		dir_norm = false
	
	last_direction = direction
	
	if manager != null:
		gridx = int(floor(translation.x/manager.cell_size))
		gridy = int(floor(translation.y/manager.cell_size))
		gridz = int(floor(translation.z/manager.cell_size))
