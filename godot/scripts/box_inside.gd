tool

extends Spatial

export (bool) var process:bool = false

var initialised:bool = false
var A_aabb:MeshInstance = null

func _process(delta):
	
	if !process:
		return
	
	if !initialised:
		initialised = true
	
	$box.transform = $A.global_transform
	$box.transform_inv = $A.global_transform.affine_inverse()
	
	$aabb.global_transform.origin = $A.global_transform.origin
	var aabb_size = $box.box_size()
	$aabb.global_transform.basis = Basis.IDENTITY.scaled( aabb_size*.5 )
	
	$normal.global_transform.origin = Vector3.ZERO
	$dir.global_transform.origin = Vector3.ZERO
	$normal.clear()
	$dir.clear()
	
	$box.direction = Vector3.UP
	$button/check.visible = $box.is_inside2( $cell.global_transform.origin )
	if $button/check.visible:
		$touch.global_transform.origin = $cell.global_transform.origin
		$projection.global_transform.origin = $box.contact
		$normal.begin(Mesh.PRIMITIVE_LINES)
		$normal.add_vertex( $box.contact )
		$normal.add_vertex( $box.contact + $box.normal )
		$normal.end()
		$dir.begin(Mesh.PRIMITIVE_LINES)
		$dir.add_vertex( $box.contact - $box.direction )
		$dir.add_vertex( $box.contact )
		$dir.add_vertex( $box.contact )
		$dir.add_vertex( $box.contact - $box.reflected_direction )
		$dir.end()
	else:
		$touch.global_transform.origin = $button.global_transform.origin
		$projection.global_transform.origin = $button.global_transform.origin
