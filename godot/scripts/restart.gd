extends Button

onready var boids:Boids = get_node( "../../boids" )

func _ready():
	self.connect("pressed",self,"restart")

func restart():
	boids.restart()
