tool

extends Node

# configuration
export (float,0,1) var margin:float = 0.04
export (int,0,10) var subdivide:int = 0

# order is important!
var vertices:Array = [ 
	# top face
	Vector3(1,1,1), Vector3(-1,1,1), Vector3(-1,-1,1), Vector3(1,-1,1), 
	# bottom face
	Vector3(1,1,-1), Vector3(-1,1,-1), Vector3(-1,-1,-1), Vector3(1,-1,-1)
	]

# in -> collider
var transform:Transform = Transform.IDENTITY
var transform_inv:Transform = Transform.IDENTITY
# in -> 3d point
var position:Vector3 = Vector3.ZERO
var direction:Vector3 = Vector3.ZERO
var radius:float = 0

# out
var contact:Vector3 = Vector3.ZERO
var normal:Vector3 = Vector3.ZERO
var reflected_direction:Vector3 = Vector3.ZERO
var inside:bool = false

# utils
func vmin( a:Vector3, b:Vector3 ):
	return Vector3( min(a.x,b.x), min(a.y,b.y), min(a.z,b.z) )

func vmax( a:Vector3, b:Vector3 ):
	return Vector3( max(a.x,b.x), max(a.y,b.y), max(a.z,b.z) )

# process
func box_size():
	var _min:Vector3 = Vector3.ZERO
	var _max:Vector3 = Vector3.ZERO
	# testing top face only
	for i in range(0,4):
		var p:Vector3 = transform.basis.xform(vertices[i])
		_min = vmin(p,_min)
		_max = vmax(p,_max)
	return  vmax( (_min*-1) , _max ) * 2

func box_inside( t:Transform, reverse:bool = false ):
	
	# testing center
	position = t.origin
	if is_inside():
		return true
		
	# testing vertices
	if subdivide <= 0:
		for v in vertices:
			position = t.basis.xform(v) + t.origin
			if is_inside():
				return true
	else:
		var gap:float = 1.0 / (subdivide+1)
		var m:float = gap
		for i in range(0,subdivide+1):
			for v in vertices:
				position = t.basis.xform(v*m) + t.origin
				if is_inside():
					return true
			m += gap
	
	if reverse:
		# storing current data
		var former:Transform = transform
		var former_inv:Transform = transform_inv
		transform = t
		transform_inv = t.affine_inverse()
		var success:bool = false
		success = box_inside( former, false )
		# restoring data
		transform = former
		transform_inv = former_inv
		return success
		
	return false

func is_inside( project:bool = false ):
	
	# creation of an inverted scale matrix
	var p:Vector3 = transform_inv.xform( position )
	if p[0] < -1-margin or p[0] > 1+margin:
		inside = false
	elif p[1] < -1-margin or p[1] > 1+margin:
		inside = false
	elif p[2] < -1-margin or p[2] > 1+margin:
		inside = false
	else:
		inside = true
	
	contact = transform.xform( p )
	
	if project:
		# there is a contact, now trying to project it to nearest plane,
		# it's important to consider the real size of the object
		# to have a distance in global space, not local
		var size:Vector3 = transform.basis.get_scale()
		var normal:Vector3 = Vector3.RIGHT
		var better_axis:int = 0
		if size[1]-abs(p[1])*size[1] < size[better_axis]-abs(p[better_axis])*size[better_axis]:
			better_axis = 1
			normal = Vector3.UP
		if size[2]-abs(p[2])*size[2] < size[better_axis]-abs(p[better_axis])*size[better_axis]:
			better_axis = 2
			normal = Vector3.FORWARD
		if p[better_axis] < 0:
			p[better_axis] = -1
			normal *= -1
		else:
			p[better_axis] = 1
		
		contact = transform.xform( p )
		
		normal = transform.basis.xform( normal ).normalized()
		var diff:Vector3 = position-contact
		var diffl:float = diff.length()
		var m:float = diffl / direction.dot(diff.normalized())
		contact = position - direction*m
		
		reflected_direction = direction * -1
		reflected_direction = reflected_direction + (normal*abs(normal.dot(reflected_direction))-reflected_direction)*2
	
	return inside

func is_inside2( pos:Vector3 ):
	
	# creation of an inverted scale matrix
	var p:Vector3 = transform_inv.xform( pos )
	if p[0] < -1 or p[0] > 1:
		return false
	if p[1] < -1 or p[1] > 1:
		return false
	if p[2] < -1 or p[2] > 1:
		return false
	
	# there is a contact, now trying to project it to nearest plane,
	# it's important to consider the real size of the object
	# to have a distance in global space, not local
	var size:Vector3 = transform.basis.get_scale()
	normal = Vector3.RIGHT
	var better_axis:int = 0
	if size[1]-abs(p[1])*size[1] < size[better_axis]-abs(p[better_axis])*size[better_axis]:
		better_axis = 1
		normal = Vector3.UP
	if size[2]-abs(p[2])*size[2] < size[better_axis]-abs(p[better_axis])*size[better_axis]:
		better_axis = 2
		normal = Vector3.FORWARD
	if p[better_axis] < 0:
		p[better_axis] = -1
	else:
		p[better_axis] = 1
		normal *= -1
	
	normal = transform.basis.xform( normal ).normalized()
	var gp:Vector3 = transform.xform( p )
	var diff:Vector3 = pos-gp
	var diffl:float = diff.length()
	var m:float = diffl / direction.dot(diff.normalized())
	contact = pos - direction*m
	
	var d = normal.dot(direction)
	var norm_part = normal * d
	var distnorm = norm_part - (direction)
	
	reflected_direction = (direction) + distnorm * 2
	
#	reflected_direction = direction * -1
#	reflected_direction = reflected_direction+(normal*abs(normal.dot(reflected_direction))-reflected_direction)*2

	return true
