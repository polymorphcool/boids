extends Camera

onready var boids:Boids = get_node("../Boids")

func _ready():
	pass # Replace with function body.

func _process(delta):
	
	if !visible:
		return
	
	if boids.amount > 0:
		var p:Vector3 = boids.get_boid_position(0)
		var fwd:Vector3 = boids.get_boid_forward(0) * -1
		var right:Vector3 = Vector3.UP.cross(fwd)
		var up:Vector3 = fwd.cross(right)
		
		global_transform.origin = boids.global_transform.origin + p
		global_transform.basis = Basis( right, up, fwd )
