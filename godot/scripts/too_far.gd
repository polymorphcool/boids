tool

extends Position3D

export (bool) var too_far:bool = false setget set_too_far

func set_too_far(b:bool):
	too_far = false
	if b:
		var c:Camera = get_viewport().get_camera()
		var v3:Vector3 = c.global_transform.origin + c.global_transform.basis[2] * c.far * 2
		translation = get_parent().to_local(v3)
