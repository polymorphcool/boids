extends Label

export(String) var address = null

onready var default_color:Color = get( "custom_styles/normal" ).bg_color 

var hovered = false
var url = null

func _ready():
	set( "custom_styles/normal", get( "custom_styles/normal" ).duplicate() )
	deactivate()
	self.connect( "mouse_entered", self, "activate" )
	self.connect( "mouse_exited", self, "deactivate" )
	if address != null:
		url = address

func activate():
	hovered = url != null
	get( "custom_styles/normal" ).bg_color = default_color

func deactivate():
	hovered = false
	get( "custom_styles/normal" ).bg_color = Color(0,0,0,0)

func _input(event):
	if url == null or not hovered:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		OS.shell_open(url)
		hovered = false
