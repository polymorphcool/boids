extends Label

onready var boids:Boids = get_node( "../../boids" )

func _process(delta):
	text = "active boids: " + str(boids.get_active_boid_amount())
