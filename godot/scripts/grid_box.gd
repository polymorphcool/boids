tool

extends Spatial

export (int,1,50) var domain_x:int = 4 setget set_domain_x
export (int,1,50) var domain_y:int = 4 setget set_domain_y
export (int,1,50) var domain_z:int = 4 setget set_domain_z
export (int,1,10) var subdivide:int = 1
export (bool) var process:bool = false

func set_domain_x(i:int):
	domain_x = i
	initialised = false

func set_domain_y(i:int):
	domain_y = i
	initialised = false

func set_domain_z(i:int):
	domain_z = i
	initialised = false

# order is important!
const vertices:Array = [ 
	# top face
	Vector3(1,1,1), Vector3(-1,1,1), Vector3(-1,-1,1), Vector3(1,-1,1), 
	# bottom face
	Vector3(1,1,-1), Vector3(-1,1,-1), Vector3(-1,-1,-1), Vector3(1,-1,-1)
	]

var initialised:bool = false
var cell_num:int = 0
var domain:Vector3 = Vector3.ZERO
var grid_size:Vector3 = Vector3.ZERO
var registration_request:int = 0
var registration_success:int = 0
var quick_skips:int = 0
var long_skips:int = 0
var box_cells:Array = []

func regenerate_grid():
	cell_num = domain_x * domain_y * domain_z
	grid_size = Vector3(domain_x,domain_y,domain_z)
	domain = Vector3(domain_x,domain_y,domain_z)
	if $cell.multimesh.instance_count != cell_num:
		$cell.multimesh.instance_count = cell_num
		$cell.multimesh.visible_instance_count = cell_num
	for z in range(0,domain_z):
		for y in range(0,domain_y):
			for x in range(0,domain_x):
				var id:int = x + y * domain_x + z * domain_x * domain_y
				var t:Transform = Transform(Basis.IDENTITY.scaled(Vector3.ONE*.5),Vector3(x,y,z) + Vector3.ONE * .5 )
				$cell.multimesh.set_instance_transform( id, t )
				$cell.multimesh.set_instance_color( id, Color(0,1,1,.2) )
	$bbox.clear()
	$bbox.begin(Mesh.PRIMITIVE_LINES)
	$bbox.add_vertex( Vector3.ZERO )
	$bbox.add_vertex( Vector3(grid_size[0],0,0) )
	$bbox.add_vertex( Vector3.ZERO )
	$bbox.add_vertex( Vector3(0,grid_size[1],0) )
	$bbox.add_vertex( Vector3.ZERO )
	$bbox.add_vertex( Vector3(0,0,grid_size[2]) )
	$bbox.add_vertex( grid_size )
	$bbox.add_vertex( Vector3(grid_size[0],grid_size[1],0) )
	$bbox.add_vertex( grid_size )
	$bbox.add_vertex( Vector3(grid_size[0],0,grid_size[2]) )
	$bbox.add_vertex( grid_size )
	$bbox.add_vertex( Vector3(0,grid_size[1],grid_size[2]) )
	$bbox.end()

func reset_grid():
	for i in range( 0,cell_num ):
		$cell.multimesh.set_instance_color( i, Color(0,1,1,.005) )

func keep_in_cell( v:Vector3 ):
	var l:float = abs(v[0]) 
	var id:int = 0
	for i in range(1,3):
		var a:float = abs(v[i])
		if l < a:
			l = a
			id = i
	if l > 1:
		return v / l
	else:
		return v

func get_cell_axis( position:Vector3 ):
	var cell_axis:Vector3 = Vector3.ZERO
	for a in range(0,3):
		if position[a] < 0:
			cell_axis[0] = -1
			break
		cell_axis[a] = int(position[a])
		if cell_axis[a] < 0 or cell_axis[a] >= domain[a]:
			cell_axis[0] = -1
			break
	return cell_axis

func get_cell_id( cell_axis:Vector3 ):
	if cell_axis[0] == -1:
		return -1
	var id:int = cell_axis[0] + cell_axis[1] * domain_x + cell_axis[2] * domain_x * domain_y
	if id >= 0 and id < cell_num:
		return id
	return -1

func set_cell_color( cell_axis:Vector3, color:Color ):
	var id:int = get_cell_id( cell_axis )
	if id != -1:
		$cell.multimesh.set_instance_color( id, color )

func register_in_grid( t:Transform ):
	
	var centers:Array = []
	centers.resize(8)
	var pts:Array = []
	pts.resize(8)
	var cell_axis:Vector3 = Vector3.ZERO
	var id:int = 0
	
	var selected:Color = Color(1,0,0)

	for i in range(0,8):
		var position:Vector3 = t.xform(vertices[i])
		# extracting cell id
		cell_axis = get_cell_axis( position )
		pts[i] = position
		centers[i] = Vector3(cell_axis[0],cell_axis[1],cell_axis[2])
	
	var diff10:Vector3 = pts[1] - pts[0]
	var dir10 = keep_in_cell( diff10 )
	var r10:float = diff10.length() / dir10.length()
	var jumps10:int = int(ceil( r10 )) * subdivide
	# reajusting the direction based on jumps
	dir10 = diff10 / jumps10
	
	var diff21:Vector3 = pts[2] - pts[1]
	var dir21 = keep_in_cell( diff21 )
	var r21:float = diff21.length() / dir21.length()
	var jumps21:int = int(ceil( r21 )) * subdivide
	# reajusting the direction based on jumps
	dir21 = diff21 / jumps21
	
	var diff40:Vector3 = pts[4] - pts[0]
	var dir40 = keep_in_cell( diff40 )
	var r40:float = diff40.length() / dir40.length()
	var jumps40:int = int(ceil( r40 )) * subdivide
	# reajusting the direction based on jumps
	dir40 = diff40 / jumps40
	
#	print( "jumps, r10: ", jumps10, ", r21: ", jumps21, ", r40: ", jumps40 )
#	print( "error, r10: ", (r10-jumps10), ", r21: ", (r21-jumps21), ", r40: ", (r40-jumps40) )
	
	var previous_id:int = -1
	var cell_list:Array = []
	for a in range(0,jumps10+1):
		var va:Vector3 = pts[0] + dir10 * a
		for b in range(0,jumps21+1):
			var vb:Vector3 = va + dir21 * b
			for c in range(0,jumps40+1):
				var vc:Vector3 = vb + dir40 * c
				id = get_cell_id( get_cell_axis( vc ) )
				registration_request += 1
				if id == -1 or id == previous_id:
					quick_skips += 1
					continue
				previous_id = id
				if !cell_list.has(id):
					cell_list.push_back(id)
					set_cell_color( get_cell_axis( vc ), selected )
					registration_success += 1
				else:
					long_skips += 1
	box_cells.append( cell_list )
#	print( "cell touched: ", cell_list.size() )
#	print( "cell_list:", cell_list )
	
	$imm.clear()
#	$imm.begin(Mesh.PRIMITIVE_LINES)
#	$imm.add_vertex( pts[0] )
#	$imm.add_vertex( pts[0] + dir10 * .5 )
#	$imm.add_vertex( pts[0] )
#	$imm.add_vertex( pts[0] + dir21 * .5 )
#	$imm.add_vertex( pts[0] )
#	$imm.add_vertex( pts[0] + dir40 * .5 )
#	$imm.add_vertex( pts[0] - (dir21+dir40)*.01 )
#	$imm.add_vertex( pts[0] - (dir21+dir40)*.01 + diff10 )
#	$imm.add_vertex( pts[0] - (diff10+dir40)*.01 )
#	$imm.add_vertex( pts[0] - (diff10+dir40)*.01 + diff21 )
#	$imm.add_vertex( pts[0] - (diff10+dir21)*.01 )
#	$imm.add_vertex( pts[0] - (diff10+dir40)*.01 + diff40 )
#	# cell centers
#	for c in centers:
#		$imm.add_vertex( c )
#		$imm.add_vertex( c + Vector3.RIGHT * .1 )
#		$imm.add_vertex( c )
#		$imm.add_vertex( c + Vector3.UP * .1 )
#		$imm.add_vertex( c )
#		$imm.add_vertex( c + Vector3.FORWARD * .1 )
#	# steps
#	var js:Vector3 = Vector3.ZERO
#	js = pts[0]
#	for i in range(0,jumps10):
#		$imm.add_vertex( js - (dir21+dir40)*.03 - (dir21+dir40)*i*.01 )
#		$imm.add_vertex( js + dir10 - (dir21+dir40)*.03 - (dir21+dir40)*i*.01 )
#		js += dir10
#	$imm.end()

func _process(delta):
	
	if !initialised:
		# regenerate grid
		regenerate_grid()
		initialised = true
	
	if !initialised or !process:
		return
	
	var start_time = OS.get_ticks_msec()
	
	reset_grid()
	
	registration_request = 0
	registration_success = 0
	quick_skips = 0
	long_skips = 0
	box_cells = []
	
	var t:Transform = Transform.IDENTITY
	t = $A.global_transform
	register_in_grid( t )
	t = $B.global_transform
	register_in_grid( t )
	t = $C.global_transform
	register_in_grid( t )
	
	var time = OS.get_ticks_msec() - start_time
	print( "\nsubdivisions: ", subdivide )
	print( "registrations: ", registration_request, ", success:", registration_success, ", quick skips: ", quick_skips, ", long_skips: ", long_skips )
	for bc in box_cells:
		print( "\tnumber cells: ", bc.size() )
	print( "process time: ", time, ' msec' )
