extends Label

onready var boids:Boids = get_node( "../../boids" )

func _process(delta):
	text = "process time: " + str(boids.get_internal_process_time()) + "ms"
