tool

extends MeshInstance

export (int,0,20) var group_id:int = 0

func _process(delta):
	global_transform.origin = $"../Boids".get_group_center(group_id)

