extends Label

onready var boids:Boids = get_node( "../../boids" )

func _process(delta):
	text = "collisions count: " + str(boids.get_collision_count())
