tool

extends ImmediateGeometry

export (bool) var wireframe:bool = false setget do_wireframe

func do_wireframe(b:bool):
	wireframe = false
	if b:
		clear()
		begin(Mesh.PRIMITIVE_LINES)
		
		add_vertex( (Vector3.RIGHT + Vector3.FORWARD + Vector3.UP)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.FORWARD + Vector3.UP)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.FORWARD + Vector3.UP)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.BACK + Vector3.UP)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.BACK + Vector3.UP)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.BACK + Vector3.UP)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.BACK + Vector3.UP)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.FORWARD + Vector3.UP)*.5 )
		
		add_vertex( (Vector3.RIGHT + Vector3.FORWARD + Vector3.UP)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.FORWARD + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.FORWARD + Vector3.UP)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.FORWARD + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.BACK + Vector3.UP)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.BACK + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.BACK + Vector3.UP)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.BACK + Vector3.DOWN)*.5 )
		
		add_vertex( (Vector3.RIGHT + Vector3.FORWARD + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.FORWARD + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.FORWARD + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.BACK + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.LEFT + Vector3.BACK + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.BACK + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.BACK + Vector3.DOWN)*.5 )
		add_vertex( (Vector3.RIGHT + Vector3.FORWARD + Vector3.DOWN)*.5 )
		
		end()
