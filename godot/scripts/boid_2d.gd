tool

extends Node2D

export (int,1,200) var max_neighbours:int = 20
export (float,0,1) var speed_randomness:float = 0.01

var direction:Vector2 = Vector2.UP
var last_direction:Vector2 = Vector2.UP
var dir_norm:bool = true

var manager:Node = null
var gridx:int = 0
var gridy:int = 0
var cellid:int = -1
var neighbours:Array = []
var curr_speed:float = 1
var speed_mult:float = 1
var separation_vec:Vector2 = Vector2.ZERO
var alignment_vec:Vector2 = Vector2.ZERO
var cohesion_vec:Vector2 = Vector2.ZERO

func _ready():
	speed_mult = rand_range(1-speed_randomness,1)

func direct_to( v:Vector2 ):
	direction = v
	last_direction = v

func move(delta:float):
	
	if manager != null:
		curr_speed += ( manager.speed * speed_mult - curr_speed ) * 15 * delta
	position += direction * curr_speed * delta
	
	if manager != null:
		if position.x >= manager.boundary.x:
			direction += Vector2.LEFT * manager.boundary_strength * delta
			dir_norm = true
		elif position.x <= -manager.boundary.x:
			direction += Vector2.RIGHT * manager.boundary_strength * delta
			dir_norm = true
		if position.y <= -manager.boundary.y:
			direction += Vector2.DOWN * manager.boundary_strength * delta
			dir_norm = true
		if position.y >= manager.boundary.y:
			direction += Vector2.UP * manager.boundary_strength * delta
			dir_norm = true
	
	var nnum:int = neighbours.size() - 1 # there is always one neighbour: self
	if manager != null and nnum > 0:
		var naccurate:int = 0
		var dmutl = manager.reactivity * delta
		separation_vec = Vector2.ZERO
		alignment_vec = Vector2.ZERO
		cohesion_vec = Vector2.ZERO
		var nsize:int = neighbours.size()
		var num:int = nsize
		var start:int = 0
		if num > max_neighbours:
			start = int(rand_range(0,num))
			num = max_neighbours
		for i in range(start,start+num):
			var n:Node2D = neighbours[i%nsize]
			if n == self:
				continue
			var dist:Vector2 = position - n.position
			var l:float = dist.length()
			var lratio:float = l / manager.cell_size
			if l < 10:
				dist = dist.normalized() * manager.cell_size * 2
			if l <= manager.cell_size:
				separation_vec += dist
				alignment_vec += n.direction * manager.cell_size * (1-lratio)
				cohesion_vec += n.position
				naccurate += 1
		if naccurate > 0:
			separation_vec /= naccurate * max(0.01, manager.sociability)
			alignment_vec /= naccurate
			cohesion_vec /= naccurate
			direction += separation_vec * 			manager.separation * dmutl
			direction += alignment_vec * 			manager.alignment * dmutl
			direction += (cohesion_vec-position) * 	manager.cohesion * dmutl
			dir_norm = true
#		var diff:Vector2 = n.position - position
#		var dsqrt:float = diff.length()
#		# pushing each other
#		if dsqrt < 10:
#			position -= diff * 30 * delta
#		# avoiding behaviour
#		if dsqrt < 200:
#			var m:float = last_direction.dot( n.last_direction )
#			m = 1 - ( (1+m)*.5 )
#			var d:float = (1-(dsqrt/200))
#			direction -= diff.normalized() * (2+m*8) * d * delta 
#			curr_speed += -curr_speed * m * d * delta
#		dir_norm = true
	
	if dir_norm:
		direction = direction.normalized()
		rotation = direction.angle()
		dir_norm = false
	
	last_direction = direction
	
	if manager != null:
		gridx = int(floor(position.x/manager.cell_size))
		gridy = int(floor(position.y/manager.cell_size))
