tool

extends Spatial

export (int,0,10000) 	var number:int = 100
export (int,0,1000) 	var cell_size:int = 100
export (Vector3) 		var boundary:Vector3 = Vector3(500,500,500)
export (bool) 			var generate:bool = false 		setget do_generate
export (bool) 			var run:bool = false

export (float,0,200) 	var speed:float = 2 			setget set_speed
export (float,0,10) 	var separation:float = 0.5 		setget set_separation
export (float,0,10) 	var alignment:float = 0.5 		setget set_alignment
export (float,0,10) 	var cohesion:float = 0.5 		setget set_cohesion
export (float,0,1) 		var sociability:float = 0.99 	setget set_sociability
export (float,0,80) 	var reactivity:float = 5	 	setget set_reactivity

export (float,0,80) 	var boundary_strength:float = 5

export (Vector3) 		var constant_force:Vector3 = Vector3.ZERO
export (Vector3) 		var direction_flattening:Vector3 = Vector3.ZERO

var initialised:bool = false
var offset:Vector2 = Vector2.ZERO
var cell:Line2D = null
var grid_offsetx:int = 0
var grid_offsety:int = 0
var grid_offsetz:int = 0
var grid_rows:int = 0
var grid_cols:int = 0
var grid_layers:int = 0
var grid_size:int = 0
var grid:Array = []

func set_separation(f:float):
	separation = f

func set_alignment(f:float):
	alignment = f

func set_cohesion(f:float):
	cohesion = f

func set_speed(f:float):
	speed = f

func set_sociability(f:float):
	sociability = f

func set_reactivity(f:float):
	reactivity = f

func do_generate(b:bool):
	generate = false
	if b:
		prepare()

func prepare():
	
	while $swarm.get_child_count() > 0:
		var c:Node = $swarm.get_child(0)
		$swarm.remove_child( c )
		c.queue_free()
	
	var tmpl:MeshInstance = $tmpl/boid
# warning-ignore:unused_variable
	for i in range(0,number):
		var b:MeshInstance = tmpl.duplicate()
		b.manager = self
		$swarm.add_child( b )
		b.visible = true
		b.material_override = b.material_override.duplicate()
		b.material_override.set_shader_param( "albedo_front", Color.from_hsv(rand_range(0,1), 1,1,1) )
		b.material_override.set_shader_param( "albedo_back", Color.from_hsv(rand_range(0,1), 1,.3,.7) )
		b.translation = boundary * Vector3(rand_range(-1,1),rand_range(-1,1),rand_range(-1,1))
		var bd:Vector2 = Vector2.RIGHT.rotated(rand_range(0,TAU)).normalized()
		b.direct_to( Vector3(bd.x,0,bd.y) )

func _ready():
	if !Engine.editor_hint:
		prepare()
		run = true

func _input(event):
	if Engine.editor_hint:
		return
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_SPACE:
			prepare()
		elif event.scancode == KEY_ESCAPE:
			get_tree().quit()

func get_cell( x:int, y:int, z:int ):
	if x < grid_offsetx or x >= grid_offsetx + grid_rows:
		return []
	elif y < grid_offsety or y >= grid_offsety + grid_cols:
		return []
	elif z < grid_offsetz or z >= grid_offsetz + grid_layers:
		return []
	else:
		return grid[ (x-grid_offsetx) + (y-grid_offsety) * grid_rows + (z-grid_offsetz) * grid_rows * grid_cols ]

func get_neighbours( boid:Spatial ):
	boid.neighbours = []
	if boid.cellid == -1:
		return
	for z in range( boid.gridz-1, boid.gridz+2 ):
		for y in range( boid.gridy-1, boid.gridy+2 ):
			for x in range( boid.gridx-1, boid.gridx+2 ):
				boid.neighbours += get_cell(x,y,z)

func _process(delta):
	
	if !run:
		return
	
	$fps.text = str(Engine.get_frames_per_second())
	
	var bb_min:Vector3 = Vector3.ZERO
	var bb_max:Vector3 = Vector3.ZERO
	for b in $swarm.get_children():
		get_neighbours( b )
		b.move( delta )
		
		if bb_min.x > b.translation.x:
			bb_min.x = b.translation.x
		if bb_max.x < b.translation.x:
			bb_max.x = b.translation.x
		
		if bb_min.y > b.translation.y:
			bb_min.y = b.translation.y
		if bb_max.y < b.translation.y:
			bb_max.y = b.translation.y
		
		if bb_min.z > b.translation.z:
			bb_min.z = b.translation.z
		if bb_max.z < b.translation.z:
			bb_max.z = b.translation.z
	
	var minx:int = int(floor(bb_min.x/cell_size))
	var maxx:int = int(ceil(bb_max.x/cell_size))
	
	var miny:int = int(floor(bb_min.y/cell_size))
	var maxy:int = int(ceil(bb_max.y/cell_size))
	
	var minz:int = int(floor(bb_min.z/cell_size))
	var maxz:int = int(ceil(bb_max.z/cell_size))
	
	grid_offsetx = minx
	grid_offsety = miny
	grid_offsetz = minz
	grid_rows = (maxx-minx)
	grid_cols = (maxy-miny)
	grid_layers = (maxz-minz)
	grid_size = grid_rows * grid_cols * grid_layers
	
	# grid resizing
	if grid.size() > grid_size:
		grid.resize( grid_size )
	elif grid.size() < grid_size:
		while grid.size() < grid_size:
			grid.append([])
	# grid cleanup
	for i in range(0,grid_size):
		grid[i] = []
	# grid registration
	for b in $swarm.get_children():
		b.cellid = (b.gridx-grid_offsetx) + (b.gridy-grid_offsety) * grid_rows + (b.gridz-grid_offsetz) * grid_rows * grid_cols
		if b.cellid >= grid_size:
			print( "too far!!! ", b.cellid, "/", grid_size )
		else:
			grid[b.cellid].append(b)
