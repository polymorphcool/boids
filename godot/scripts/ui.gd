extends Node2D

const domain_borders:Array = ['none','bounce','push','attract','respawn','teleport','kill']
var controls:Array = []
var initialised:bool = false

func get_param( param:String ):
	return $main/Boids.get( param )

func set_param( param:String, v ):
	return $main/Boids.set( param, v )

func float2text( f:float ):
	return str( int(f*100) * 0.01 )

func update_label( c:Dictionary ):
	if c.control is Slider:
		if c.label != null:
			match c.param:
				"domain_border":
					c.label.text = domain_borders[int(c.control.value)]
				_:
					c.label.text = float2text( c.control.value )

func add_control( name:String, param:String, root:Node ):
	if root.get_node( name ) == null:
		return
	var c = {
		'control': root.get_node( name ),
		'label': root.get_node( name + "_lbl" ),
		'delay': false,
		'timer': 0,
		'init': get_param( param ),
		'param': param
	}
	if "param" == "amount":
		c.delay = true
		
	if c.control is Slider:
		c.control.value = c.init
		update_label(c)
		c.control.connect( "changed_better", self, "slider_changed" )
	elif c.control is CheckBox:
		c.control.pressed = c.init
		c.control.connect( "changed_pressed", self, "checkbox_changed" )
		
	controls.append( c )

func slider_changed( emitor:Slider, f:float ):
	for c in controls:
		if c.control == emitor:
			update_label(c)
			if !c.delay:
				set_param( c.param, f )
			else:
				c.timer = 1

func checkbox_changed( emitor:CheckBox, b:bool ):
	for c in controls:
		if c.control == emitor:
			if !c.delay:
				set_param( c.param, b )
			else:
				c.timer = 1

func fullscreen():
	OS.set_window_fullscreen(!OS.window_fullscreen)
	if OS.window_fullscreen:
		$panels/bg/all/actions/fullscreen.text = "./windowed"
		OS.set_window_maximized(false) 
	else:
		$panels/bg/all/actions/fullscreen.text = "./fullscreen"
		OS.set_window_maximized(false) 
		OS.set_window_size(Vector2(1280,720))

func reset():
	for c in controls:
		if c.control is Slider:
			c.control.value = c.init
		elif c.control is CheckBox:
			c.control.pressed = get_param( c.param )

func far():
#	$main/cam_rider.visible = false
#	$main/cam_rider.set_current(false)
#	$main/cam_pivot/cam.set_current(true)
	pass

func ride():
#	$main/cam_pivot/cam.set_current(false)
#	$main/cam_rider.visible = true
#	$main/cam_rider.set_current(true)
	pass

func quit():
	get_tree().quit()

func mouse_pass( root:Node ):
	for c in root.get_children():
		if not c is Control:
			pass
		var ctrl:Control = c
		if ctrl.mouse_filter == Control.MOUSE_FILTER_STOP:
			ctrl.mouse_filter = Control.MOUSE_FILTER_PASS

func menu_visibility():
	$panels/bg/all/domain.visible = !$panels/bg/all/domain.visible
	$panels/bg/all/boids.visible = $panels/bg/all/domain.visible
	$panels/bg/all/controls.visible = $panels/bg/all/domain.visible
	$panels/bg/all/info.visible = $panels/bg/all/domain.visible
	if $panels/bg/all/domain.visible:
		$panels/bg/all/actions/menu.text = "./menu hide"
	else:
		$panels/bg/all/actions/menu.text = "./menu show"
	$panels/bg.rect_size = Vector2.ZERO

func _ready():
	
# warning-ignore:return_value_discarded
	$panels/bg/all/actions/menu.connect("pressed", self, "menu_visibility")
# warning-ignore:return_value_discarded
	$panels/bg/all/actions/fullscreen.connect("pressed", self, "fullscreen")
# warning-ignore:return_value_discarded
	$panels/bg/all/actions/reset.connect("pressed", self, "reset")
# warning-ignore:return_value_discarded
	$panels/bg/all/actions/far.connect("pressed", self, "far")
# warning-ignore:return_value_discarded
	$panels/bg/all/actions/ride.connect("pressed", self, "ride")
# warning-ignore:return_value_discarded
	$panels/bg/all/actions/quit.connect("pressed", self, "quit")
	
	mouse_pass( $panels/bg/all/actions )
	mouse_pass( $panels/bg/all/domain )
	mouse_pass( $panels/bg/all/boids )

func _input(event):
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_ESCAPE:
				quit()
			KEY_BACKSPACE:
				quit()
			KEY_BACKSLASH:
				quit()
			KEY_F4:
				quit()

func link_all():
	# domain
	add_control( "cell_size", "cell_size", $panels/bg/all/domain )
	add_control( "domain_x", "domain_x", $panels/bg/all/domain )
	add_control( "domain_y", "domain_y", $panels/bg/all/domain )
	add_control( "domain_z", "domain_z", $panels/bg/all/domain )
	add_control( "domain_border", "domain_border", $panels/bg/all/domain )
	add_control( "border_radius", "domain_border_radius", $panels/bg/all/domain )
	add_control( "domain_pull", "domain_pull", $panels/bg/all/domain )
	add_control( "domain_visible", "domain_visible", $panels/bg/all/domain )
	# boids
	add_control( "amount", "amount", $panels/bg/all/boids )
	add_control( "speed", "speed", $panels/bg/all/boids )
	add_control( "max_test", "max_boid_tests", $panels/bg/all/boids )
	add_control( "radius", "boid_radius", $panels/bg/all/boids )
	add_control( "separation", "separation_weight", $panels/bg/all/boids )
	add_control( "alignment", "alignment_weight", $panels/bg/all/boids )
	add_control( "cohesion", "cohesion_weight", $panels/bg/all/boids )

func _process(delta):
	
	if !initialised:
		initialised = true
		link_all()
		return
	
	$panels/bg/all/actions/fps.text = str( int( Engine.get_frames_per_second() * 100 ) * 0.01 ) + "fps"
	
	for c in controls:
		if !c.delay:
			continue
		elif c.timer > 0:
			c.timer -= delta
			if c.timer <= 0:
				if c.control is Slider:
					set_param( c.param, c.control.value )
				elif c.control is CheckBox:
					set_param( c.param, c.control.pressed )
